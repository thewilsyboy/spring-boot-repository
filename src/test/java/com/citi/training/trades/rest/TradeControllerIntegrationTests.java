package com.citi.training.trades.rest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;


import com.citi.training.trades.model.Trade;

public class TradeControllerIntegrationTests {
	private static final Logger logger = LoggerFactory.getLogger(TradeControllerIntegrationTests.class);

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void getTrade_returnsTrade() {
        restTemplate.postForEntity("/trade", new Trade(1, "apple", 2.3, 34), Trade.class);

        ResponseEntity<List<Trade>> getAllResponse = restTemplate.exchange(
                "/trade", HttpMethod.GET, null,
                new ParameterizedTypeReference<List<Trade>>() {});

        logger.info("getAllTrade response: " + getAllResponse.getBody());

        assertEquals(HttpStatus.OK, getAllResponse.getStatusCode());
        assertTrue(getAllResponse.getBody().get(0).getStock().equals("apple"));
        assertEquals(getAllResponse.getBody().get(0).getPrice(), 23, 0.002);
        assertEquals(getAllResponse.getBody().get(0).getVolume(), 124);

        // It's not easy to rollback the transactions here as we're interfacing to a web server
        // instead, we'll delete the trade we created.
        restTemplate.delete("/trade/" + getAllResponse.getBody().get(0).getId());
    }

    @Test
    public void getEmployee_returnsNotFound() {

        ResponseEntity<Trade> getByIdResponse = restTemplate.exchange(
                "/trade/9999", HttpMethod.GET, null,
                new ParameterizedTypeReference<Trade>() {});

        logger.info("getByIdEmployee response: " + getByIdResponse.getBody());

        assertEquals(HttpStatus.NOT_FOUND, getByIdResponse.getStatusCode());
    }

}
