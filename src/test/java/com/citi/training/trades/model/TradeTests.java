package com.citi.training.trades.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.citi.training.trades.model.Trade;

public class TradeTests {
	
	private int testID = 1;
	private String testStock = "APPLE";
	private double testPrice = 23.4;
	private int testVolume = 22;
	
	public void test_Trade_constructor() {
		Trade testTrade = new Trade(testID, testStock, testPrice, testVolume);
		
		assertEquals(testID, testTrade.getId());
		assertEquals(testStock, testTrade.getStock());
		assertEquals(testPrice, testTrade.getPrice(), 0.0001);
		assertEquals(testVolume, testTrade.getVolume());
	}
	@Test
    public void test_Trade_toString() {
        String testString = new Trade(testID, testStock, testPrice, testVolume).toString();
        
        assertTrue(testString.contains((new Integer(testID)).toString()));
        assertTrue(testString.contains((new Integer(testVolume)).toString()));
        assertTrue(testString.contains(String.valueOf(testStock)));
        assertTrue(testString.contains(String.valueOf(testPrice)));
    }
}


