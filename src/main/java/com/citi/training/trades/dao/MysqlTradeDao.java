package com.citi.training.trades.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import com.citi.training.trades.dao.TradeDao;
import com.citi.training.trades.model.Trade;



@Component
public class MysqlTradeDao implements TradeDao {
	
	@Autowired
    JdbcTemplate tpl;
	
	public List<Trade> findAll() {
        return tpl.query("SELECT id, stock, price, volume FROM trade",
                         new TradeMapper());
	}
	public void deleteById(int id) {
        findById(id);
        tpl.update("DELETE FROM trade WHERE id=?", id);
    }
	public Trade findById(int id) {
        List<Trade> trades = tpl.query(
                "SELECT id, stock, price, volume FROM trade WHERE id=?",
                new Object[] {id},
                new TradeMapper()
        );
        
        if(trades.size() <= 0) {
        	System.out.println("Trade not found");
            //throw new TradeNotFoundException("Employee with id=[" + id +
                                                 //"] not found");
        }
        
        return trades.get(0);
        
    }
	public Trade create(Trade trade) {
        KeyHolder keyHolder = new GeneratedKeyHolder();

        tpl.update(
           new PreparedStatementCreator() {
               @Override
               public PreparedStatement createPreparedStatement(Connection connection)
                                                                   throws SQLException {

                   PreparedStatement ps =
                           connection.prepareStatement(
                                   "insert into trade (stock, price, volume) values (?, ?, ?)",
                           Statement.RETURN_GENERATED_KEYS);
                   ps.setString(1,  trade.getStock());
                   ps.setDouble(2,  trade.getPrice());
                   ps.setInt(3, trade.getVolume());
                   return ps;

               }
           },
           keyHolder
         );
        trade.setId(keyHolder.getKey().intValue());
        return trade;
    }
	private static final class TradeMapper implements RowMapper<Trade>{
    	/**
    	 * Assigns a unique row number to each trade created in the database
    	 */

        public Trade mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new Trade(rs.getInt("id"),
            					rs.getString("stock"),
                                rs.getDouble("price"),
                                rs.getInt("volume"));
        }

    }
}
