package com.citi.training.trades.dao;

import java.util.List;

import org.springframework.stereotype.Component;

import com.citi.training.trades.model.Trade;

@Component
public interface TradeDao {
	
	List<Trade> findAll();
	
	Trade findById(int id);
	
	Trade create(Trade trade);
	
	void deleteById(int id);
}
